
def main_menu():
    print("digite '1' pra efetuar CADASTROS")
    print("digite '2' pra efetuar ALTERAÇÕES")
    print("digite '3' pra efetuar REMOÇÕES")
    print("digite '4' pra efetuar LEITURA")

def registration_menu():
    print("Para cadastrar um novo ALUNO, digite 'a'")
    print("Para cadastrar um novo CURSO, digite 'c'")
    print("Para cadastrar um novo PROFESSOR, digite 'p'")
    print("Para cadastrar uma nova DISCIPLINA, digite 'd'")

def change_menu():
    print("Para alterar o curso de um aluno, digite 'ca'")
    print("Para alterar a matrícula de um aluno em uma disciplina, digite 'md'")
    print("Para alterar o professor responsável pela disciplina, digite 'pd'")

def removal_menu():
    print("Para alterar o curso de um aluno, digite 'ca'")
    print("Para remover a matrícula de um aluno em uma disciplina, digite 'md'")

def reading_menu():
    print("Para fazer a consulta de todos os alunos cadastrados na faculdade, digite '1'")
    print("Para fazer a consulta de todos os professores cadastrados na faculdade, digite'2'")
    print("Para fazer a consulta de um aluno específico, digite '3'")
    print("Para fazer a consulta dos alunos associados a um curso, digite '4'")
    print("Para fazer a consulta das disciplinas com mais matriculados, digite '5'")
