
import sqlite3
from uuid import uuid4

def registration_student():

    connection = sqlite3.connect("./database.db.db")
    client = connection.cursor()  
         
    cpf = input("CPF do aluno: ")
    name = input('Nome do aluno: ')
    age = int(input("Idade do aluno: "))
    rg = input("RG: ") or None
    dispatching_agency = input("Orgão expedidor: ") or None
    student_id = str(uuid4())
    db_rg = f'"{rg}"' if rg is not None else "NULL"
    db_dispatching_agency = f'"{dispatching_agency}"' if dispatching_agency is not None else "NULL"
    
    client.execute(f'INSERT INTO alunos values("{student_id}", "{cpf}", "{name}", {age}, {db_rg}, {db_dispatching_agency});')
    connection.commit()
    print("Você cadastrou o novo aluno")


def registration_course():
    
    connection = sqlite3.connect("./database.db.db")
    client = connection.cursor()      

    name = input("Nome do curso: ")
    creation = int(input('Data da criação do curso: '))
    building_class = input("Prédio onde se leciona as aulas: ")
    course_id = str(uuid4())

    client.execute(f'INSERT INTO cursos values("{course_id}", "{name}", "{creation}", "{building_class}")')
    connection.commit()
    print("Você cadastrou um novo curso")
    

def registration_teacher():
    
    connection = sqlite3.connect("./database.db.db")
    client = connection.cursor()      
    cpf = input("CPF do professor: ")
    name = input('Nome do professor: ')
    degree_training = input("Titulação do professor: ")
    research_area = input("área de pesquisa: ") or None
    teacher_id = str(uuid4())
    db_research_area = f'"{research_area}"' if research_area is not None else "NULL"

    client.execute(f'INSERT INTO professores values("{teacher_id}", "{cpf}", "{name}", "{degree_training}", {db_research_area});')
    connection.commit()
    print("Você cadastrou o novo professor")


def registration_discipline():
    
    connection = sqlite3.connect("./database.db.db")
    client = connection.cursor() 
      
    code = input('Código da disciplina: ')
    name = input('Nome da disciplina: ')
    description = input("Dê uma breve descrição sobre a disciplina: ")
    workload = input("Carga horária da disciplina: ")
    discipline_id = str(uuid4())

    client.execute(f'INSERT INTO disciplinas values("{discipline_id}", "{code}", "{name}", "{description}", "{workload}");')
    connection.commit()
    print("Você cadastrou uma nova disciplina")